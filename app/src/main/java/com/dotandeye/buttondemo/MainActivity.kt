package com.dotandeye.buttondemo

import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    image.alpha = 1f
                    true
                }
                MotionEvent.ACTION_UP -> {
                    image.alpha = 0.5f
                    v.performClick()
                    true
                }
                else -> false
            }
        }

        // ทดสอบ
    }
}